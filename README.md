
1: Klonirati repo:

    git clone https://gitlab.com/mpericic/iolap.git

2: Instalirati dependencies:

    cd iolap && npm install

3: popuniti knexfile.js ispravnim podacima za spajanje na pg bazu

  

4: kreirati i popuniti bazu

    npx knex migrate:latest

    npx knex seed:run

5: pokrenuti development server

    npm run dev:serv

6: pokrenuti development client

    npm run dev:cli

7: Ako je sve spremno za production build pokrenuti

    npm run build

nakon cega su izbuildani file-ovi u /lib direktoriju

8: Pokrenuti prod build sa

    npm run start

