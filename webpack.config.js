const path = require('path')
const glob = require('glob')
const NODE_ENV = process.env.NODE_ENV || 'development';
const IS_DEVELOPMENT = NODE_ENV === 'development';
const IS_PRODUCTION = NODE_ENV === 'production';

const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');


var plugins = []
if (IS_DEVELOPMENT) {
    plugins.push(
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body'
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        })
    )
}

else {
    plugins.push(
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    );

}
opt = function () {
    if (IS_PRODUCTION) {
        return {
            usedExports: true
        }
    }
}
module.exports = {
    entry: ['@babel/polyfill', './src/client/index.js'],
    devtool: IS_DEVELOPMENT ? 'eval' : 'cheap-source-map',
    optimization: opt(),
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }, {
                test: /\.css$/,
                use: "style-loader"
            }, {
                test: /\.(eot|woff|woff2|ttf|svg|gif|png|jpg|jpeg|ico)$/,
                loader: "file-loader"
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    plugins: plugins,
    output: {
        path: __dirname + '/lib/public',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        historyApiFallback: true,
        contentBase: '/',
        hot: true,
        compress: true,
        port: 8080,
        host: '0.0.0.0'
    }
};