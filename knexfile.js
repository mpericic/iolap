require('@babel/register');
module.exports = {
    development: {
        client: 'postgresql',
        connection: {
            port: 5432,
            host: 'localhost',
            database: 'knex',
            user: 'mpericic',
            password: '',
        },
        pool: {
            min: process.env.DATABASE_POOL_MIN,
            max: process.env.DATABASE_POOL_MAX,
        },
        migrations: {
            directory: './src/db/migrations',
            tableName: 'knex_migrations',
        },
        seeds: {
            directory: './src/db/seeds',
        },
    },
    production: {
        client: 'postgresql',
        connection: {
            port: 5432,
            host: 'localhost',
            database: 'knex',
            user: 'mpericic',
            password: '',
        },
        pool: {
            min: process.env.DATABASE_POOL_MIN,
            max: process.env.DATABASE_POOL_MAX,
        },
        migrations: {
            directory: './src/db/migrations',
            tableName: 'knex_migrations',
        },
        seeds: {
            directory: './src/db/seeds',
        }
    }
}
