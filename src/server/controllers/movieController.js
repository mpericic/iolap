"use strict";
import knex from 'knex';
import Movie from './../models/Movie'
const movieController = {
	getAll: async (req, res) => {
		try {
			var data = await Movie.query()
			res.json({ success: true, data: data })
		}
		catch (e) {
			res.json({ err: true, data: e })
		}
	},
	getOne: async (req, res) => {
		try {
			var data = await Movie.query().findById(req.params.id)
			res.json({ success: true, data: data })
		}
		catch (e) {
			res.json({ err: true, data: e })
		}
	},
	update: async (req, res) => {
		var { id, name, genre, rating, explicit } = { ...req.body }
		try {
			var data = await Movie.query().updateAndFetchById(id, { name, genre, rating, explicit })
			return res.json(data)
		}
		catch (e) {
			res.json({ err: true, data: e })
		}
	},
	create: async (req, res) => {
		var { name, genre, rating, explicit } = req.body
		try {
			var data = await Movie.query().insert({ name, genre, rating, explicit })
			res.json(data)
		}
		catch (e) {
			res.json({ err: true, data: e })
		}
	},
	delete: async (req, res) => {
		try {
			var data = await Movie.query().delete().where('id', '=', req.params.id)
			if (data) {
				res.json({ success: true })
			}
			else {
				res.json({ err: true, data: 'no such movie' })
			}
		}
		catch (e) {
			res.json({ err: true, data: e })
		}
	},

}

export default movieController;

