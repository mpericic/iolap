"use strict";
import '@babel/polyfill'

import app from './app';
import config from './../config/config'
var cfg = config.server[process.env.NODE_ENV]
const server = app.listen(cfg.port, '127.0.0.1', () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log('App is at http://%s:%s', host, port);
});

export default server