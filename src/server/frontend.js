import express from 'express';
import path from 'path';

var fe = express()
fe.use('/', express.static(path.resolve(__dirname, '../../lib/public')))

export default fe;