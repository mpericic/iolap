import { Model } from 'objection'
import connection from './../../db/knex'

Model.knex(connection)

class Movie extends Model {
    static get tableName() {
        return 'movies';
    }

}

export default Movie;