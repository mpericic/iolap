"use strict";
import express from 'express';
import movieController from './controllers/movieController'
var movieRouter = express.Router()

movieRouter.get('/', movieController.getAll);
movieRouter.get('/:id', movieController.getOne);
movieRouter.put('/:id', movieController.update);
movieRouter.post('/', movieController.create);
movieRouter.delete('/:id', movieController.delete);


export default movieRouter;