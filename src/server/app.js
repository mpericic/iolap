"use strict";

import express from 'express'
import cors from 'cors'

import movieRouter from './routes';
import bodyParser from 'body-parser';
import fe from './frontend'

var app = express()
app.use(bodyParser())
app.use(cors())

app.use('/api/v1/movies', movieRouter);
app.use('/', fe);

export default app;