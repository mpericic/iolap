import React, { Component } from 'react';
import ReactDOM, { render } from 'react-dom';
import Layout from './js/Layout'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import allReducers from './js/reducers';

if (module.hot) {
    module.hot.accept();
}
const store = createStore(
    allReducers,
    applyMiddleware(thunk)
);

const rootEl = document.getElementById('root');

ReactDOM.render(<Provider store={store}>
    <Layout />
</Provider>, rootEl
)
