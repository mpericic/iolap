import update from 'immutability-helper';

import { MOVIE_LIST, MOVIE_ADDING, MOVIE_ADD, MOVIE_UPDATE, MOVIE_DELETE, UPDATING_MOVIE, CANCEL } from '../actions'

const initialState = {
	moviesList: []
}

export function moviesReducer(state = initialState, action) {
	switch (action.type) {
		case MOVIE_LIST:
			return update(state, {
				moviesList: { $set: action.payload }
			})
		case MOVIE_ADD:
			return update(state, {
				creating: { $set: false },
				moviesList: { $push: [action.payload] }
			})
		case MOVIE_ADDING:
			return update(state, {
				creating: { $set: true }
			})
		case MOVIE_UPDATE:
			return update(state, {
				updating: { $set: false },
				moviesList: {
					$apply: (val) => val.map((item => {
						if (item.id === action.payload.id) {
							return action.payload
						}
						else {
							return item
						}
					}))
				}
			})
		case MOVIE_DELETE:
			return update(state, {
				moviesList: arr => arr.filter(item => item.id !== action.payload.id)
			})
		case UPDATING_MOVIE:
			return update(state, {
				movieToUpdate: { $set: action.payload },
				updating: { $set: true }
			})
		case CANCEL:
			return update(state, {
				updating: { $set: false },
				creating: { $set: false },
				movieToUpdate: { $set: false }
			})
		default: return state
	}
}
