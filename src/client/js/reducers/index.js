import { combineReducers } from 'redux';
import { moviesReducer } from './movie';

const allReducers = combineReducers({
    movie: moviesReducer
});

export default allReducers