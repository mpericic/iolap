import React, { Component } from 'react';
import {connect} from 'react-redux';
import App from './App'
import BootstrapProvider from '@bootstrap-styled/provider';
const myTheme = {
  '$btn-primary-bg': 'blue',
  '$btn-primary-color': 'white',
};
class Layout extends Component {
  constructor(props){
    super(props)
    this.state={navSmall:false}
  }
  render() {
    return (
			<BootstrapProvider theme={myTheme}>
        <App />
      </BootstrapProvider>
    );
  }
}

function mapStateToProps(state) {
    return {
        app: state.app,
        user: state.user
    };
}

export default connect(mapStateToProps)(Layout)
