import React, { Component, useRef } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { loadMovies } from './fetch/movie'
import { updatingMovie, addingMovie } from './actions'
import { movieDelete } from './fetch/movie'
import { Form } from './Form'
import Container from '@bootstrap-styled/v4/lib/Container';
import Button from '@bootstrap-styled/v4/lib/Button';
import ListGroup from '@bootstrap-styled/v4/lib/ListGroup';
import ListGroupItem from '@bootstrap-styled/v4/lib/ListGroup/ListGroupItem';
import Col from '@bootstrap-styled/v4/lib/Col';

class App extends Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    this.props.loadMovies()
  }
  render() {
    return (
      <>
        <Container>
          <Container>
            <Button onClick={() => this.props.addingMovie()}>Add new</Button>
          </Container>
          <br />
          {this.props.movie.creating || this.props.movie.updating ? <Form /> : null}
          <br />
          <Container>
            <ListGroup>
              <ListGroupItem style={{ backgroundColor: 'rgba(30,20,199, 0.7)' }}>
                <Col >name</Col>
                <Col >genre</Col>
                <Col>rating</Col>
                <Col>explicit</Col>
                <Col />
              </ListGroupItem>
              {this.props.movie.moviesList ? this.props.movie.moviesList.map(movie => <ListGroupItem key={movie.id}>
                <Col>{movie.name}</Col>
                <Col>{movie.genre}</Col>
                <Col>{movie.rating}</Col>
                <Col>{movie.explicit === true ? 'yes' : 'no'}</Col>
                <Button onClick={() => this.props.updatingMovie(movie)}>edit</Button>
                <Button onClick={() => this.props.deleteMovie(movie)}>delete</Button>
              </ListGroupItem>) : null}

            </ListGroup>
          </Container>
        </Container>
      </>
    )
  }
}

function mapStateToProps(state) {
  return {
    movie: state.movie
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    loadMovies: loadMovies,
    updatingMovie: (movie) => updatingMovie(movie),
    addingMovie: addingMovie,
    deleteMovie: movieDelete
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(App)
