export const MOVIE_LIST = 'MOVIE_LIST'
export const MOVIE_ADD = 'MOVIE_ADD'
export const MOVIE_UPDATE = 'MOVIE_UPDATE'
export const MOVIE_DELETE = 'MOVIE_DELETE'
export const UPDATING_MOVIE = 'UPDATING_MOVIE'
export const CANCEL = 'CANCEL'
export const MOVIE_ADDING = 'MOVIE_ADDING'

export function movieList(data) {
	return {
		type: MOVIE_LIST,
		payload: data.data
	}
}
export const addMovie = (data) => {
	return {
		type: MOVIE_ADD,
		payload: data
	}
}
export const addingMovie = () => {
	return {
		type: MOVIE_ADDING
	}
}

export const updateMovie = (data) => {
	return {
		type: MOVIE_UPDATE,
		payload: data
	}
}
export const updatingMovie = (data) => {
	return {
		type: UPDATING_MOVIE,
		payload: data
	}
}
export const deleteMovie = (data) => {
	return {
		type: MOVIE_DELETE,
		payload: data
	}
}

export const cancelUpdateSave = () => {
	return {
		type: CANCEL
	}
}