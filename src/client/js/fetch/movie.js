import { movieList, updateMovie, addMovie, deleteMovie } from '../actions';
import config from './../../../config/config'
const urlData = config.server[process.env.NODE_ENV]
const url = 'http://' + urlData.url + ':' + urlData.port + '/api/v1/movies/'
export function loadMovies() {
	return async dispatch => {
		let req = await fetch(url)
		let data = await req.json()
		dispatch(movieList(data))
	}
}

export function movieDelete(data) {
	return async dispatch => {
		try {
			let req = await fetch(url + data.id, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json'
				}
			})
			let result = await req.json()
			if (result.success) {
				dispatch(deleteMovie(data))
			}

		}
		catch (e) {
			console.log(e)
		}
	}
}
export function movieUpdate(data) {
	return async dispatch => {
		try {
			let req = await fetch(url + data.id, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data)
			})
			let result = await req.json()
			dispatch(updateMovie(result))
		}
		catch (e) {
			console.log(e)
		}
	}
}

export function movieCreate(data) {
	data.explicit = data.explicit ? true : false
	console.log(data)
	return async dispatch => {
		try {
			let req = await fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data)
			})
			let result = await req.json()
			dispatch(addMovie(result))
		}
		catch (e) {
			console.log(e)
		}
	}
}