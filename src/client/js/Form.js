import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { updatingMovie, cancelUpdateSave } from './actions'
import { movieUpdate, movieCreate } from './fetch/movie'
import Container from '@bootstrap-styled/v4/lib/Container';
import InputGroup from '@bootstrap-styled/v4/lib/InputGroup';
import InputGroupAddon from '@bootstrap-styled/v4/lib/InputGroup/InputGroupAddon';
import InputGroupButton from '@bootstrap-styled/v4/lib/InputGroup/InputGroupButton';
import Input from '@bootstrap-styled/v4/lib/Input';
import Label from '@bootstrap-styled/v4/lib/Label';
export const Form = () => {
    const movie = useSelector(state => state.movie)
    const dispatch = useDispatch()
    var data
    if (movie.updating) {
        data = { ...movie.movieToUpdate }
    }
    else {
        data = {}
    }
    const onChange = (a, b) => {
        var value
        if (b === 'explicit') {
            value = a.target.checked
        }
        else {
            value = a.target.value
        }
        data[b] = value
        if (movie.updating) {
            dispatch(updatingMovie(data))
        }
    }
    return (
        <Container>
            <InputGroup>
                <Input placeholder="name" type="text" onChange={(a) => onChange(a, 'name')} value={movie.updating || movie.creating ? data.name : ''} />
                <Input placeholder="genre" type="text" onChange={(a) => onChange(a, 'genre')} value={movie.updating || movie.creating ? data.genre : ''} />
                <Input placeholder="rating" type="number" onChange={(a) => onChange(a, 'rating')} value={movie.updating || movie.creating ? data.rating : ''} />
                <InputGroupAddon>
                    <Label>explicit</Label>
                    <Input addon type="checkbox" onChange={(a) => onChange(a, 'explicit')} checked={data.explicit} />
                </InputGroupAddon>
                {movie.updating ? <InputGroupButton onClick={() => { dispatch(movieUpdate(data)) }}>Update</InputGroupButton> : null}
                {!movie.updating ? <InputGroupButton onClick={() => { dispatch(movieCreate(data)) }}>Save</InputGroupButton> : null}
                <InputGroupButton onClick={() => dispatch(cancelUpdateSave())}>Cancel</InputGroupButton>
            </InputGroup>
        </Container >
    )
}