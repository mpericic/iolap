const config = {
	db: {
		user: 'postgres',
		pass: '',
		db: 'knex',
		host: 'localhost',
		port: 5432
	},
	server: {
		development: {
			port: 3000,
			url: '127.0.0.1'
		},
		production: {
			port: 3000,
			url: '127.0.0.1'
		}
	},
	client: {
		port: 8080,
		address: '127.0.0.1'
	}
}
export default config;