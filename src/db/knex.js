var environment = process.env.NODE_ENV || 'development'
import knex from 'knex';
import envs from './../../knexfile'
const connection = knex(envs[environment])

export default connection